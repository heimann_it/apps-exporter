*******************************************************************************
 Program to Export Oracle Applications-Data via FNDLOAD
*******************************************************************************
 Author          : Gordon Heimann (Gordon@Heimann.IT)
 Created         : 13.11.2003
 Last Update     : 12.09.2020
 Current Version : 0.9.9
*******************************************************************************
 This little Tool enables you to directly export several Application Objects
 into a so called .ldt File, by using the Oracle FNDLOAD Functionality.

 Please Excuse, that the Concurrent Program ist currently only available in 
 English. Maybe one of the next Versions will include other languages, too.

 If you wish to use and enhance this file with more Object Types,
 please send a copy to gordon@heimann.it
 Also feel free to contact me for any questions concerning the program.

 Using the Tool is strictly on your own risk. I will not be realiable
 for any damage that may have been caused using this tool.
*******************************************************************************
  Version History:
     0.9   13.11.2003 (GordonHeimann)    Initial Version
     0.9.1 24.05.2004 (GordonHeimann)    added Functions,Menus,Resps
     0.9.2 15.05.2006 (EllenPommerening) added XML Datasources
     0.9.3 01.08.2006 (EnnoPommerening)  added jtfgrid.lct - CRM Foundation Spreadtable Meta Data
     0.9.4 16.08.2007 (Gordon Heimann)   removed params Methods, they caused platform problems
                                         added a second .ldt-file for 11.5.9
     0.9.5 19.02.2008 (Gordon Heimann)   added NEW_OWNER Parameter to replace Owner while copy
                                         translated Concurent Program Parameters to english
     0.9.6 06.08.2008 (Gordon Heimann)   added Key Flexfield 
     0.9.7 22.12.2010 (Gordon Heimann)   corrections in Readme.txt
     0.9.8 01.06.2020 (Gordon Heimann)   added 12.2.1 ldt to upload concurrent program
                                         added Forms Personalizations
     0.9.9 12.09.2020 (Gordon Heimann)   added Descriptive Flexfield

*******************************************************************************
 Installation Manual:
*******************************************************************************
 To install the Applications Export please perform the following steps:
 0.  Be sure to execute the following Steps with the apropriate Oracle Apps User
 1.  Copy the file "xx_download_apps" to the "bin" directory under the desired
     Application path. (e.g. $XBOL_TOP/bin)
 2a. If you install to Application "XBOL" skip this Step.
     If you choose to install the Programm to a different Application, edit the
     File "xx_download_apps.ldt" by Replacing the Application "XBOL" with your
     desired Application Short Name (Use an editor and the Find&Replace Function)
 2b. The Concurrent Program will load with the Name "XX Apps-Objekte entladen".
     If you wish to have a different name, please change the File
     "xx_download_apps.ldt" accordingly
 3.  Depending on the Apps Version you are using, use the corresponding file
     (xx_download_apps_<version>.ldt) and rename it to xx_download_apps.ldt.
 4.  Copy the file "xx_download_apps.ldt" to the "bin" directory under the desired
     Application path. (e.g. $XBOL_TOP/bin)
 5.  Copy the file "upload_conc_prog.sh" to the "bin" directory under the desired
     Application path. (e.g. $XBOL_TOP/bin)
 6.  Change the Access Mode of the copied Files to be able to Read and Execute 
     (eg. 755 should be suitable) the files.
 7.  Change connection string (user/pwd@database) in file "upload_conc_prog.sh"
     to the one fitting to your Environment.
 8.  Start the File "upload_conc_prog.sh". It will import the Concurrent Program
     into Applications.
 9.  Login to Applications "System Administrator" Responsibility and add the
     Concurrent Program "XX Apps-Objekte entladen" (or the Name that you chose
     in Step 2b.) to the desired Request Group.

Now, you successfully installed The Applications Exporter
*******************************************************************************